package com.jasindo.crud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jasindo.crud.model.DataModel;
import com.jasindo.crud.repository.DataRepository;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/userdata")
public class DataController {

    @Autowired
    DataRepository dataRepo;

    // Could've add pagination here
    @GetMapping("/")
    public ResponseEntity<List<DataModel>> getAllUserData() {
        try {
            List<DataModel> data = new ArrayList<DataModel>();

            dataRepo.findAll().forEach(data::add);

            if (data.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(data, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<DataModel> getUserDataById(@PathVariable("id") long id) {
        DataModel data = dataRepo.findById(id);

        if (data != null) {
            return new ResponseEntity<>(data, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<String> createUserData(@RequestBody DataModel data) {
        try {
            dataRepo.add(new DataModel(data.getId(), data.getFirstName(), data.getLastName(), data.getEmail()));
            return new ResponseEntity<>("User data was created successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateUserData(@PathVariable("id") long id, @RequestBody DataModel data) {
        DataModel dataUser = dataRepo.findById(id);

        if (dataUser != null) {
            dataUser.setId(id);
            dataUser.setFirstName(data.getFirstName());
            dataUser.setLastName(data.getLastName());
            dataUser.setEmail(data.getEmail());

            dataRepo.update(dataUser);
            return new ResponseEntity<>("User data was updated successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Cannot find User data with id=" + id, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteDataUser(@PathVariable("id") long id) {
        try {
            int result = dataRepo.deleteById(id);
            if (result == 0) {
                return new ResponseEntity<>("Cannot find Data user with id= " + id, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>("Data user was deleted successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot delete Data user.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteAllDataUser() {
        try {
            int numRows = dataRepo.deleteAll();
            return new ResponseEntity<>("Deleted " + numRows + " User data(s) successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot delete User data.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
