package com.jasindo.crud.repository;

import java.util.List;

import com.jasindo.crud.model.DataModel;

public interface DataRepository {

    DataModel findById(Long id);

    List<DataModel> findAll();
    
    List<DataModel> findByName(String firstName, String lastName);

    int add(DataModel data);

    int update(DataModel data);

    int deleteById(Long id);

    int deleteAll();

}
