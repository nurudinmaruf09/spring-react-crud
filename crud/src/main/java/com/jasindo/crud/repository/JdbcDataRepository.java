package com.jasindo.crud.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.jasindo.crud.model.DataModel;

@Repository
public class JdbcDataRepository implements DataRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    //GET Method by id
    @Override
    public DataModel findById(Long id) {
        try {
            DataModel data = jdbcTemplate.queryForObject(
                    "SELECT * from data_user WHERE id=?",
                    BeanPropertyRowMapper.newInstance(DataModel.class), id);

            return data;
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }

    //GET Method for all data
    @Override
    public List<DataModel> findAll() {
        return jdbcTemplate.query(
                "SELECT * from data_user ORDER BY id",
                BeanPropertyRowMapper.newInstance(DataModel.class));
    }

    //Searching data user by name
    @Override
    public List<DataModel> findByName(String firstName, String lastName) {
        String q = "SELECT * from data_user WHERE first_name ILIKE '%" + firstName + "%' OR last_name ILIKE ILIKE '%" + lastName + "%'";
        return jdbcTemplate.query(q, BeanPropertyRowMapper.newInstance(DataModel.class));
    }

    //POST Method for new data
    @Override
    public int add(DataModel data) {
        return jdbcTemplate.update("INSERT INTO data_user (first_name, last_name, email) VALUES(?,?,?)",
                new Object[] { data.getFirstName(), data.getLastName(), data.getEmail() });
    }

    //PUT Method by id
    @Override
    public int update(DataModel data) {
        return jdbcTemplate.update("UPDATE data_user SET first_name=?, last_name=?, email=? WHERE id=?",
                new Object[] { data.getFirstName(), data.getLastName(), data.getEmail(), data.getId() });
    }
    
    //DELETE Method by id
    @Override
    public int deleteById(Long id) {
        return jdbcTemplate.update("DELETE FROM data_user WHERE id=?", id);
    }

    //DELETE Method all data
    @Override
    public int deleteAll() {
        return jdbcTemplate.update("DELETE from data_user");
    }

}
