drop table data_user;

CREATE TABLE data_user (
    id BIGSERIAL PRIMARY KEY NOT NULL,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    email VARCHAR(50)
);