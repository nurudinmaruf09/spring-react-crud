import React, { useState } from 'react'
import './App.css'
import ListDataUserComponent from './components/ListDataUserComponent'
import HeaderComponent from './components/HeaderComponent'
import FooterComponent from './components/FooterComponent'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import AddDataUserComponent from './components/AddDataUserComponent'

function App() {

  return (
    <>
    <BrowserRouter>
      <HeaderComponent/>
      <Routes>
        <Route path="/" element = {<ListDataUserComponent />}></Route>
        <Route path="/add-datauser" element = {<AddDataUserComponent/>}></Route>
        <Route path="/edit-datauser/:id" element = {<AddDataUserComponent/>}></Route>

      </Routes>
      <FooterComponent/>
    </BrowserRouter>
    </>
  )
}

export default App
