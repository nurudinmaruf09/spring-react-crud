import React, { useEffect, useState } from "react";
import { createDataUser, getDataUserbyId, updateDataUser } from "../services/DataUser";
import { useNavigate, useParams } from "react-router-dom";

const AddDataUserComponent = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");

  const {id} = useParams();
  const [errors, setErrors] = useState({
    firstName: "",
    lastName: "",
    email: "",
  });

  const navigator = useNavigate();

  useEffect(() => {
    if(id){
      getDataUserbyId(id).then((response) => {
        setFirstName(response.data.firstName);
        setLastName(response.data.lastName);
        setEmail(response.data.email);
      }).catch(error => {
        console.error(error);
      })
    }
  }, {id})

  function validateForm() {
    let valid = true;

    const errorsCopy = { ...errors };

    if (firstName.trim()) {
      errorsCopy.firstName = "";
    } else {
      errorsCopy.firstName = "First name is required";
      valid = false;
    }

    if (lastName.trim()) {
      errorsCopy.lastName = "";
    } else {
      errorsCopy.lastName = "Last name is required";
      valid = false;
    }

    if (email.trim()) {
      errorsCopy.email = "";
    } else {
      errorsCopy.email = "Email is required";
      valid = false;
    }

    setErrors(errorsCopy);

    return valid;
  }

  function addOrUpdateDataUser(e) {
    e.preventDefault();

    if (validateForm()) {

      const dataUser = { firstName, lastName, email };
      console.log(dataUser);

      if(id){
        updateDataUser(id, dataUser).then((response) => {
          console.log(response.data);
          navigator("/");
        }).catch(error => {
          console.error(error);
        });
      } else {
        createDataUser(dataUser).then((response) => {
          console.log(response.data);
          navigator("/");
        }).catch(error => {
          console.log(response.data);
        });
      }

    }
  }

  function pageTitle(){
    if(id){
      return <h2 className="text-center mt-3">Update User Data</h2>
    } else{
      <h2 className="text-center mt-3">Add User Data</h2>
    }
  }

  return (
    <div className="container position-relative pt-5 mb-5">
      <div className="row align-items-center pb-5 pt-5">
        <div className="card col-md-6 offset-md-3 offset-md-3 mt-3 mb-3">
          {
            pageTitle()
          }
          <div className="card-body">
            <form>
              {/* First Name form input */}
              <div className="form-group mb-2">
                <label className="form-label">First Name</label>
                <input
                  type="text"
                  placeholder="Enter User first name"
                  name="firstName"
                  value={firstName}
                  className={`form-control ${
                    errors.firstName ? "is-invalid" : ""
                  }`}
                  onChange={(e) => setFirstName(e.target.value)}
                />
                {errors.firstName && (
                  <div className="invalid-feedback"> {errors.firstName} </div>
                )}
              </div>

              {/* Last Name form input */}
              <div className="form-group mb-2">
                <label className="form-label">Last Name</label>
                <input
                  type="text"
                  placeholder="Enter User last name"
                  name="lastName"
                  value={lastName}
                  className={`form-control ${
                    errors.lastName ? "is-invalid" : ""
                  }`}
                  onChange={(e) => setLastName(e.target.value)}
                />
                {errors.lastName && (
                  <div className="invalid-feedback"> {errors.lastName} </div>
                )}
              </div>

              {/* Email form input */}
              <div className="form-group mb-2">
                <label className="form-label">Email</label>
                <input
                  type="text"
                  placeholder="Enter User email"
                  name="email"
                  value={email}
                  className={`form-control ${
                    errors.email ? "is-invalid" : ""
                  }`}
                  onChange={(e) => setEmail(e.target.value)}
                />
                {errors.email && (
                  <div className="invalid-feedback"> {errors.email} </div>
                )}
              </div>

              {/* Submit form input */}
              <div className="d-grid gap-2 col-6 mx-auto">
                <button
                  className="btn btn-success text-center mt-3 mb-2 py-3"
                  onClick={addOrUpdateDataUser}
                >
                  Submit User Data
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddDataUserComponent;
