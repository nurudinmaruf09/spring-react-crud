import React, { useEffect, useState } from "react";
import { deleteDataUserById, listDataUser } from "../services/DataUser";
import { useNavigate } from "react-router-dom";

const ListDataUserComponent = () => {
  const [data, setData] = useState([]);
  const [search, setSearch] = useState("");
  const [filteredData, setFilteredData] = useState([]);

  const navigator = useNavigate();

  useEffect(() => {
    getAllDataUser();
  }, []);

  // Get all data from backend and update state
  function getAllDataUser() {
    listDataUser()
      .then((response) => {
        setData(response.data);
        setFilteredData(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  function addNewDataUser() {
    navigator("/add-datauser");
  }

  function updateDataUser(id) {
    navigator(`/edit-datauser/${id}`);
  }

  function removeDataUser(id) {
    console.log(id);
    deleteDataUserById(id)
      .then((response) => {
        getAllDataUser();
      })
      .catch((error) => {
        console.error(error);
      });
  }

  const handleInputChange = (e) => {
    const searchTerm = e.target.value;
    setSearch(searchTerm);

    // filter the items using the apiUsers state
    const filteredItems = data.filter((user) =>
      user.firstName.toLowerCase().includes(searchTerm.toLowerCase())
    );

    setFilteredData(filteredItems);
  };

  return (
    <div className="table-responsive container p-0">
      <h1 className="text-center mt-3">List of Data User</h1>
      <form>
        <div className="mb-3">
          <input
            type="text"
            placeholder="Search name"
            name="searchName"
            className="form-control"
            value={search}
            onChange={handleInputChange}
          />
        </div>
      </form>
      <button className="btn btn-success mb-2" onClick={addNewDataUser}>
        Add Data User
      </button>
      <table className="table table-striped table-hover">
        <thead className="thead-light">
          <tr className="text-center">
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody className="text-center">
          {filteredData.length === 0 ? (
            <p>No users found</p>
          ) : (
            filteredData.map((dataUser) => (
              <tr key={dataUser.id}>
                <td>{dataUser.id}</td>
                <td>{dataUser.firstName}</td>
                <td>{dataUser.lastName}</td>
                <td>{dataUser.email}</td>
                <td>
                  <button
                    className="btn btn-info"
                    onClick={() => updateDataUser(dataUser.id)}
                  >
                    Update
                  </button>
                  <button
                    className="btn btn-danger"
                    onClick={() => removeDataUser(dataUser.id)}
                    style={{ marginLeft: "10px" }}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))
          )}
          {/* {data
            // .filter((dataUser) => {
            //   return search.toLowerCase() === ""
            //     ? dataUser
            //     : dataUser.firstName.toLowerCase().startWith(String(search));
            // })
            .map((dataUser) => (
              <tr key={dataUser.id}>
                <td>{dataUser.id}</td>
                <td>{dataUser.firstName}</td>
                <td>{dataUser.lastName}</td>
                <td>{dataUser.email}</td>
                <td>
                  <button
                    className="btn btn-info"
                    onClick={() => updateDataUser(dataUser.id)}
                  >
                    Update
                  </button>
                  <button
                    className="btn btn-danger"
                    onClick={() => removeDataUser(dataUser.id)}
                    style={{ marginLeft: "10px" }}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))} */}
          <tr></tr>
        </tbody>
      </table>
    </div>
  );
};

export default ListDataUserComponent;
