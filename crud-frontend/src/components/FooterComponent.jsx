import React, { Component } from 'react'

class FooterComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
                 
        }
    }

    render() {
        return (
            <div className="bottom-0">
                <footer className = "footer text-center">
                    <span>All Rights Reserved, Est. 2024</span>
                </footer>
            </div>
        )
    }
}

export default FooterComponent