import axios from "axios";

const DATAUSER_API_BASE_URL = "http://localhost:8080/api/userdata/";

export const listDataUser = () => {
  return axios.get(DATAUSER_API_BASE_URL);
};

export const getDataUserbyId = (dataUserId) => {
  return axios.get(DATAUSER_API_BASE_URL + dataUserId);
};

export const createDataUser = (dataUser) => {
  return axios.post(DATAUSER_API_BASE_URL + "add", dataUser);
};

export const updateDataUser = (dataUserId, dataUser) => {
  return axios.put(DATAUSER_API_BASE_URL + "update/" + dataUserId, dataUser);
};

export const deleteDataUserById = (dataUserId) => {
  return axios.delete(DATAUSER_API_BASE_URL + "delete/" + dataUserId);
};

